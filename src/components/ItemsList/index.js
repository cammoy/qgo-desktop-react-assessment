import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeItem } from '../../logic/todos';
import { markAsComplete } from '../../logic/completed';
import { toggleCompleted } from '../../logic/toggle';

import './styles.css';
import Button from '../Inputs/Button';
import Checkbox from '../Inputs/Checkbox';

export const ItemsList = props => {

   const { items,  onRemove, onToggleItem, onToggleCompleted, completed, toggle } = props;
  
  const renturnedItems = () => {
    
    if(toggle) {
      return items.filter(todo => !completed.includes(todo.id));
    }
    return items;
  }


  return (
    <div className="todos">
      <Button className="todos__toggle" onClick={() => onToggleCompleted()}>
        <span className="todos__item__toggle">
          {toggle? 'show' : 'hide'}
        </span> Completed Items
      </Button>
      <ul className="todos__list">
        {items.length < 1 && <p id="items-missing">Add some tasks above.</p>}
        {renturnedItems().map(item => {
          const { id, content } = item;
          return (
            <li  className="todos__list__item" key={id}>{content}
              <Button className="todos__list__item__remove" onClick={ () => onRemove(item)}>&times;</Button>
              <Checkbox className="todos__list__item__toggle" onChange={ () => onToggleItem(id)} />
            </li>
            )
          })
        }
      </ul>
    </div>
  );
};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  items: state.todos.items, 
  completed: state.completed,
  toggle: state.toggle
});

const mapDispatchToProps = dispatch => ({
  onRemove: item => dispatch(removeItem(item)),
  onToggleItem: item => dispatch(markAsComplete(item)),
  onToggleCompleted: items => dispatch(toggleCompleted(items))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
