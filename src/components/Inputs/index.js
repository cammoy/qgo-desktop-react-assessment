import Button from './Button';
import Checkbox from './Checkbox';

export default {
    Checkbox,
    Button
};