import React from 'react';
import PropTypes from 'prop-types';


const Checkbox = ({ name, checked, onChange, className }) => (
    <input 
        type="checkbox" 
        id={name} name={name} 
        checked={checked} 
        onChange={onChange}
        className={className}
    />
);

Checkbox.defaultProps = ({
    checked: null,
    onChange: () => null
});

Checkbox.propTypes = ({
    checked: PropTypes.string,
    onChange: PropTypes.func
})

export default Checkbox;
