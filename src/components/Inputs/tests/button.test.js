import React from 'react';
import { shallow, mount } from 'enzyme';
import Comp from '../Button';

const defaultProps = {
    type: 'button', 
    className: 'todos__toggle'
};

const wrapper = shallow((
      <Comp {...defaultProps}>
        <div className="unique" />
      </Comp>
    ));

describe('Button', () => {

  it('renders without crashing', () => {
    shallow(<Comp />);
  });

  it('renders children when passed in', () => {
    expect(wrapper.contains(<div className="unique" />)).toEqual(true);
  });

  it('renders button type if passed', () => {
    expect(wrapper.props()).toHaveProperty('type', 'button');
  });

  it('simulates click events', () => {
    const renderedItem = mount(<Comp {...defaultProps} />);
    renderedItem.find('button.todos__toggle').simulate('click');
  });

  it('renders given class', () => {
    expect(wrapper.find('button').hasClass('todos__toggle')).toEqual(true);
  });

});
