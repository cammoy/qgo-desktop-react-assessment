import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ onClick, type, children, className }) => (
    <button type={type} onClick={onClick} className={className}>
        {children}
    </button>
);

Button.defaultProps = ({
    type: null,
    onClick: () => null
});

Button.propTypes = ({
    type: PropTypes.oneOf(['button', 'submit', 'reset']),
    onClick: PropTypes.func,
    children: PropTypes.node
})

export default Button;