export const TOGGLE_COMPLETED = 'qgo/assessment/';

export const toggleCompleted = () => ({ 
    type: TOGGLE_COMPLETED
});


const reducer = (state = false, action) => {
  switch (action.type) {
    case TOGGLE_COMPLETED:
    const toggle = state? false : true
    return toggle;
        
    default:
      return state;
  }
};

export default reducer;