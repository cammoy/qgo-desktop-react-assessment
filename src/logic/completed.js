export const CHECK_ITEM = 'qgo/assessment/CHECK_ITEM';
export const TOGGLE_COMPLETED_ITEMS = 'qgo/assessment/';

export const markAsComplete = id => ({ 
    type: CHECK_ITEM, id 
});


const reducer = (state = [], action) => {
    const { type, id } = action;
  switch (type) {
    case CHECK_ITEM:

      const newState = [...state];

      // Toggle Completed Tasks
      //-----------------------

      if( !newState.includes(id) ) {
          newState.push(id)
      } else {
            const index= newState.indexOf(id);
            newState.splice(index, 1);
      }
        
      return [...newState];
        
    default:
      return state;
  }
};

export default reducer;
