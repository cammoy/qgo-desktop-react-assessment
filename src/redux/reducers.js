import { combineReducers } from 'redux';
import todos from '../logic/todos';
import completed from '../logic/completed';
import toggle from '../logic/toggle';

export default function createReducer() {
  return combineReducers({
    todos,
    completed,
    toggle
  });
}
