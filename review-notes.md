## Kwasi Brown Todo App Test Evaluation.

Hi, thank you for taking the time to review my test.

## Approach

My approach was to analyse the project setup and get some familiarity with codebase, conventions etc.
After reviewing the codebase including checking package.json to see what was included in terms of scripts, and modules, I then checked the requirements and began coding.

I tried to follow the conventions of the project as much as possible in order to minimise refactoring.
Whilst extending the project I tried to keep the redux store as minimalistic as possible.

## Given more time
Possible improvements:
 - Write more tests e.g checkbox
 - Add new todo on enter
 - Move List items into its own stateless component
 - Fix small checkbox toggle issue
 - Utilise flexbox and add grid etc
 - Use sass variable for spacing elements and adding colours etc (Styled components)
 - Add animation 
 - Add search functionality
 - Add a modal that acts as details page for more detailed todo items
 - add end to end tests etc